<?php

/**
 * Implements hook_form_FORM_ID_alter().
 */
function webform_dynamic_confirmation_form_webform_configure_form_alter(&$form, &$form_state, $form_id) {
  
  // Add a new option to the redirection options on the webform settings page
  $form['submission']['redirection']['redirect']['#options']['dynamic'] = t('Dynamic URL');

  // Add a new fieldset for the "dynamic" option's settings
  $form['submission']['redirection']['redirect_dynamic'] = array(
  	'#type' => 'fieldset',
  );

  $res = webform_dynamic_confirmation_redirects($form['#node']->nid);
 
  // Dynamic URL field
  $form['submission']['redirection']['redirect_dynamic']['redirect_dynamic_field'] = array(
  	'#type' => 'select',
  	'#title' => t('Dynamic URL Field'),
  	'#description' => t('If "Dynamic URL" is chosen above, select the field which will determine the confirmation page.'),
  	'#options' => get_webform_selects($form),
  );
  if(isset($res['cid'])) {
    $form['submission']['redirection']['redirect_dynamic']['redirect_dynamic_field']['#default_value'] = $res['cid'];
    $form['submission']['redirection']['redirect']['#default_value'] = 'dynamic';
  }

  // Dynamic URLs
  $form['submission']['redirection']['redirect_dynamic']['redirect_dynamic_urls'] = array(
  	'#type' => 'textarea',
  );
  if(isset($res['confirmation'])) {
    $form['submission']['redirection']['redirect_dynamic']['redirect_dynamic_urls']['#default_value'] = $res['confirmation'];
  }

  // Add our submit handler to save values in our own table
  $form['#submit'][] = 'webform_dynamic_confirmation_admin_submit';

}

/**
 * Get a list of select fields from a form
 * @return
 *   Array keyed on cid, values are field names
 */
function get_webform_selects($form) {

  $select_fields = array();
  $components = $form['#node']->webform['components'];
  foreach($components as $component) {
  	if('select' === $component['type']) {
      $select_fields[$component['cid']] = $component['name'];
  	}
  }  
  return $select_fields;

}

/**
 * Submit handler for the webform settings form.
 */
function webform_dynamic_confirmation_admin_submit(&$form, &$form_state) {

  $redirect = $form_state['values']['redirect'];

  if('dynamic' == $redirect) {
    $redirect_field = $form_state['values']['redirect_dynamic_field'];
    $redirect_urls = $form_state['values']['redirect_dynamic_urls'];
    
    $res = db_merge('webform_dynamic_confirmation')
           ->key(array('nid' => $form['#node']->nid))
           ->fields(array(
             'cid' => $redirect_field,
             'confirmation' => $redirect_urls,
            ))->execute();
  }
  else {
    $res = db_delete('webform_dynamic_confirmation')
           ->condition('nid', $form['#node']->nid)
           ->execute();
  }

}